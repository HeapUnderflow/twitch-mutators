# Twitch Mutators

![Program Example Image](app.png)

## What the f*** is this ?
This is a small tool you can use for your very own Twitch Sellout,  
Simply head over to the [releases page] and download the latest version.

## How the f*** do i use this ?
You know how to start  files, yes ?  
Great, use that knowledge ! 

(for the real "how to", i may get around to making a wiki)

## License
This Project is licensed under the [BSD 3-clause Clear License]

[releases page]: https://gitlab.com/HeapUnderflow/twitch-mutators/releases
[BSD 3-clause Clear License]: https://gitlab.com/HeapUnderflow/twitch-mutators/blob/master/LICENSE

