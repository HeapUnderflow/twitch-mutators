﻿using System.Collections.Generic;
using Mutators.Model;
using TwitchLib.Client.Enums;

namespace Mutators.Ui
{
    class EditMutatorArgs
    {
        public int PPL;
        public int MaxMutators;
        public Set Set;
        public Dictionary<SubscriptionPlan, int> SubLevels;

        public EditMutatorArgs(Set set, Dictionary<SubscriptionPlan, int> plans, int ppl, int mxmt)
        {
            Set = set;
            SubLevels = plans;
            PPL = ppl;
            MaxMutators = mxmt;
        }
    }
}
