﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mutators.Model;

namespace Mutators.Ui
{
    public partial class ConfigureSets : Form
    {
        private int sel = -1;
        private EditSetsArgs Args;
        private Action<EditSetsArgs> _callback;

        public ConfigureSets(EditSetsArgs args, Action<EditSetsArgs> callback)
        {
            InitializeComponent();
            Icon = Utils.RandIco();
            Args = args;
            _callback = callback;
            PopulateList();

        }

        private void ConfigureSets_FormClosed(object sender, FormClosedEventArgs e)
        {
            _callback.Invoke(Args);
        }

        private void btnNewSet_Click(object sender, EventArgs e)
        {
            if (newSetName.Text.Length <= -1) return;

            var current = chkCopyCurrent.Checked ? Args.Sets[Args.Selected] : Set.Empty(Guid.Empty, null);
            var name = newSetName.Text;
            var nset = Set.Create(name, new Dictionary<Guid, Mutator>(current.Mutator), new Dictionary<Guid, Bits>(current.Bits));

            Args.Sets.Add(nset.Id, nset);
            Args.Selected = nset.Id;

            chkCopyCurrent.Checked = false;
            newSetName.Clear();
            PopulateList();
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            if (sel <= -1) return;

            var v = Args.Sets.Values.ElementAt(sel).Id;

            if (v == Guid.Empty)
            {
                if (MessageBox.Show("You cannot remove the default set, you can clear it instead ?\n"+
                                    "(The default is used as a fallback, and thus needs to be there)", 
                        "Default Set",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    Args.Sets[v].Mutator.Clear();
                    Args.Sets[v].Bits.Clear();
                    listSets.ClearSelected();
                    sel = -1;
                };

                return;
            }

            Args.Sets.Remove(v);
            Args.Selected = Guid.Empty;
            PopulateList();
            sel = -1;
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            if (nameRename.Text.Length < 1 || sel <= -1) return;

            var nt = nameRename.Text;

            var id = Args.Sets.Values.ElementAt(sel).Id;
            Args.Sets[id].Name = nt;

            nameRename.Clear();
            PopulateList();
            sel = -1;
        }

        private void btnLoadSelected_Click(object sender, EventArgs e)
        {
            if (sel <= -1) return;
            Args.Selected = Args.Sets.Values.ElementAt(sel).Id;
            PopulateList();
        }

        private void listSets_SelectedIndexChanged(object sender, EventArgs e)
        {
            sel = listSets.SelectedIndex;
        }

        private void PopulateList()
        {
            listSets.ClearSelected();
            listSets.Items.Clear();
            foreach (var v in Args.Sets.Values)
            {
                var sl = v.Id == Args.Selected ? "*" : "";
                listSets.Items.Add($"{sl}{v.Name} [{v.Mutator.Count} Mutators][{v.Bits.Count} Bits]");
            }
        }
    }
}
