﻿namespace Mutators.Ui
{
    partial class ConfigureBits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureBits));
            this.bitsList = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddBits = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numBits = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numPoints = new System.Windows.Forms.NumericUpDown();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.chkClampScaling = new System.Windows.Forms.CheckBox();
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPoints)).BeginInit();
            this.SuspendLayout();
            // 
            // bitsList
            // 
            this.bitsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bitsList.FormattingEnabled = true;
            this.bitsList.Location = new System.Drawing.Point(218, 12);
            this.bitsList.Name = "bitsList";
            this.bitsList.Size = new System.Drawing.Size(386, 199);
            this.bitsList.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddBits);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numBits);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numPoints);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 126);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Bits";
            // 
            // btnAddBits
            // 
            this.btnAddBits.Location = new System.Drawing.Point(6, 97);
            this.btnAddBits.Name = "btnAddBits";
            this.btnAddBits.Size = new System.Drawing.Size(188, 23);
            this.btnAddBits.TabIndex = 2;
            this.btnAddBits.Text = "Add";
            this.btnAddBits.UseVisualStyleBackColor = true;
            this.btnAddBits.Click += new System.EventHandler(this.btnAddBits_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Points";
            // 
            // numBits
            // 
            this.numBits.Location = new System.Drawing.Point(6, 19);
            this.numBits.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numBits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numBits.Name = "numBits";
            this.numBits.Size = new System.Drawing.Size(120, 20);
            this.numBits.TabIndex = 5;
            this.numBits.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Bits are equal to";
            // 
            // numPoints
            // 
            this.numPoints.Location = new System.Drawing.Point(6, 58);
            this.numPoints.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPoints.Name = "numPoints";
            this.numPoints.Size = new System.Drawing.Size(120, 20);
            this.numPoints.TabIndex = 3;
            this.numPoints.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(6, 188);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(200, 23);
            this.btnRemoveSelected.TabIndex = 8;
            this.btnRemoveSelected.Text = "Remove Selected";
            this.btnRemoveSelected.UseVisualStyleBackColor = true;
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // chkClampScaling
            // 
            this.chkClampScaling.AutoSize = true;
            this.chkClampScaling.Location = new System.Drawing.Point(11, 139);
            this.chkClampScaling.Name = "chkClampScaling";
            this.chkClampScaling.Size = new System.Drawing.Size(127, 17);
            this.chkClampScaling.TabIndex = 9;
            this.chkClampScaling.Text = "Use Clamped Scaling";
            this.chkClampScaling.UseVisualStyleBackColor = true;
            this.chkClampScaling.CheckedChanged += new System.EventHandler(this.chkClampScaling_CheckedChanged);
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(11, 162);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(79, 17);
            this.chkEnable.TabIndex = 10;
            this.chkEnable.Text = "Enable Bits";
            this.chkEnable.UseVisualStyleBackColor = true;
            this.chkEnable.CheckedChanged += new System.EventHandler(this.chkEnable_CheckedChanged);
            // 
            // ConfigureBits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 225);
            this.Controls.Add(this.chkEnable);
            this.Controls.Add(this.chkClampScaling);
            this.Controls.Add(this.btnRemoveSelected);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bitsList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigureBits";
            this.Text = "ConfigureBits";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConfigureBits_FormClosed);
            this.Load += new System.EventHandler(this.ConfigureBits_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPoints)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox bitsList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAddBits;
        private System.Windows.Forms.NumericUpDown numPoints;
        private System.Windows.Forms.NumericUpDown numBits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.CheckBox chkClampScaling;
        private System.Windows.Forms.CheckBox chkEnable;
    }
}