﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mutators.Ui
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            Icon = Utils.RandIco();
            pictureBox1.Image = Utils.RandImg();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www.twitch.tv/decc");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/HeapUnderflow/twitch-mutators");
        }
    }
}
