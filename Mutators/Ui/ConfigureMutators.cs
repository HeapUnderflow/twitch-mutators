﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mutators.Model;
using TwitchLib.Client.Enums;

namespace Mutators.Ui
{
    internal partial class ConfigureMutators : Form
    {
        private readonly Action<EditMutatorArgs> _callback;
        private readonly EditMutatorArgs _args;

        public ConfigureMutators(EditMutatorArgs args, Action<EditMutatorArgs> callback)
        {
            InitializeComponent();
            Icon = Utils.RandIco();
            _callback = callback;
            _args = args;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (newMutatorName.Text.Length < 0)
            {
                return;
            }

            var nn = newMutatorName.Text;
            var en = newMutatorEnabled.Checked;

            newMutatorName.Clear();
            newMutatorEnabled.Checked = true;

            _args.Set.Add(Mutator.Create(nn, en));
            
            PopulateList();
        }

        private void btnEnableSel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mtListBox.Items.Count; i++)
            {
                if (!mtListBox.GetItemChecked(i)) continue;

                var item = (Mutator) mtListBox.Items[i];
                _args.Set.Mutator[item.Id].Enabled = true;
            }

            ClearSelection();
            PopulateList();
        }

        private void btnDisableSel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mtListBox.Items.Count; i++)
            {
                if (!mtListBox.GetItemChecked(i)) continue;

                var item = (Mutator)mtListBox.Items[i];
                _args.Set.Mutator[item.Id].Enabled = false;
            }

            ClearSelection();
            PopulateList();
        }

        private void btnRemoveSel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mtListBox.Items.Count; i++)
            {
                if (!mtListBox.GetItemChecked(i)) continue;

                var item = (Mutator)mtListBox.Items[i];
                _args.Set.Remove(item.Id);
            }

            ClearSelection();
            PopulateList();
        }

        private void ConfigureMutators_Load(object sender, EventArgs e)
        {
            PopulateList();
            numPrime.Value = _args.SubLevels[SubscriptionPlan.Prime];
            numTier1.Value = _args.SubLevels[SubscriptionPlan.Tier1];
            numTier2.Value = _args.SubLevels[SubscriptionPlan.Tier2];
            numTier3.Value = _args.SubLevels[SubscriptionPlan.Tier3];
            numPPL.Value = _args.PPL;
            numMaxMt.Value = _args.MaxMutators;

        }

        private void ConfigureMutators_FormClosed(object sender, FormClosedEventArgs e)
        {
            _callback?.Invoke(_args);
        }

        private void ClearSelection()
        {
            for (var i = 0; i < mtListBox.Items.Count; i++)
            {
                mtListBox.SetItemChecked(i, false);
            }
        }

        private void PopulateList()
        {
            mtListBox.Items.Clear();
            foreach (var i in _args.Set.Mutator.Values)
            {
                mtListBox.Items.Add(i);
            }
        }

        private void numPrime_ValueChanged(object sender, EventArgs e)
        {
            _args.SubLevels[SubscriptionPlan.Prime] = (int) numPrime.Value;
        }

        private void numTier1_ValueChanged(object sender, EventArgs e)
        {
            _args.SubLevels[SubscriptionPlan.Tier1] = (int) numTier1.Value;
        }

        private void numTier2_ValueChanged(object sender, EventArgs e)
        {
            _args.SubLevels[SubscriptionPlan.Tier2] = (int) numTier2.Value;
        }

        private void numTier3_ValueChanged(object sender, EventArgs e)
        {
            _args.SubLevels[SubscriptionPlan.Tier3] = (int) numTier3.Value;
        }

        private void numPPL_ValueChanged(object sender, EventArgs e)
        {
            _args.PPL = (int) numPPL.Value;
        }

        private void numMaxMt_ValueChanged(object sender, EventArgs e)
        {
            _args.MaxMutators = (int) numMaxMt.Value;
        }
    }
}
