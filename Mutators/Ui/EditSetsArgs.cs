﻿using System;
using System.Collections.Generic;
using Mutators.Model;

namespace Mutators.Ui
{
    public class EditSetsArgs
    {
        public Dictionary<Guid, Set> Sets;
        public Guid Selected;

        public EditSetsArgs(Dictionary<Guid, Set> sets, Guid selected)
        {
            Sets = sets;
            Selected = selected;
        }
    }
}
