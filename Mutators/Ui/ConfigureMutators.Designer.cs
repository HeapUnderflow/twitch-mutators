﻿namespace Mutators.Ui
{
    partial class ConfigureMutators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureMutators));
            this.mtGrp = new System.Windows.Forms.GroupBox();
            this.newMutatorEnabled = new System.Windows.Forms.CheckBox();
            this.newMutatorName = new System.Windows.Forms.TextBox();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.mtListBox = new System.Windows.Forms.CheckedListBox();
            this.btnEnableSel = new System.Windows.Forms.Button();
            this.btnDisableSel = new System.Windows.Forms.Button();
            this.btnRemoveSel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numTier3 = new System.Windows.Forms.NumericUpDown();
            this.numTier2 = new System.Windows.Forms.NumericUpDown();
            this.numTier1 = new System.Windows.Forms.NumericUpDown();
            this.numPrime = new System.Windows.Forms.NumericUpDown();
            this.numPPL = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numMaxMt = new System.Windows.Forms.NumericUpDown();
            this.mtGrp.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTier3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTier2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTier1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxMt)).BeginInit();
            this.SuspendLayout();
            // 
            // mtGrp
            // 
            this.mtGrp.Controls.Add(this.newMutatorEnabled);
            this.mtGrp.Controls.Add(this.newMutatorName);
            this.mtGrp.Controls.Add(this.btnAddNew);
            this.mtGrp.Location = new System.Drawing.Point(13, 13);
            this.mtGrp.Name = "mtGrp";
            this.mtGrp.Size = new System.Drawing.Size(200, 103);
            this.mtGrp.TabIndex = 0;
            this.mtGrp.TabStop = false;
            this.mtGrp.Text = "Add Mutator";
            // 
            // newMutatorEnabled
            // 
            this.newMutatorEnabled.AutoSize = true;
            this.newMutatorEnabled.Checked = true;
            this.newMutatorEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.newMutatorEnabled.Location = new System.Drawing.Point(7, 47);
            this.newMutatorEnabled.Name = "newMutatorEnabled";
            this.newMutatorEnabled.Size = new System.Drawing.Size(65, 17);
            this.newMutatorEnabled.TabIndex = 2;
            this.newMutatorEnabled.Text = "Enable?";
            this.newMutatorEnabled.UseVisualStyleBackColor = true;
            // 
            // newMutatorName
            // 
            this.newMutatorName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.newMutatorName.Location = new System.Drawing.Point(7, 20);
            this.newMutatorName.Name = "newMutatorName";
            this.newMutatorName.Size = new System.Drawing.Size(187, 20);
            this.newMutatorName.TabIndex = 1;
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(6, 70);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(188, 23);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Add New";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // mtListBox
            // 
            this.mtListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mtListBox.FormattingEnabled = true;
            this.mtListBox.Location = new System.Drawing.Point(219, 13);
            this.mtListBox.Name = "mtListBox";
            this.mtListBox.Size = new System.Drawing.Size(410, 349);
            this.mtListBox.TabIndex = 1;
            // 
            // btnEnableSel
            // 
            this.btnEnableSel.Location = new System.Drawing.Point(12, 122);
            this.btnEnableSel.Name = "btnEnableSel";
            this.btnEnableSel.Size = new System.Drawing.Size(96, 23);
            this.btnEnableSel.TabIndex = 1;
            this.btnEnableSel.Text = "Enable Selected";
            this.btnEnableSel.UseVisualStyleBackColor = true;
            this.btnEnableSel.Click += new System.EventHandler(this.btnEnableSel_Click);
            // 
            // btnDisableSel
            // 
            this.btnDisableSel.Location = new System.Drawing.Point(113, 122);
            this.btnDisableSel.Name = "btnDisableSel";
            this.btnDisableSel.Size = new System.Drawing.Size(100, 23);
            this.btnDisableSel.TabIndex = 2;
            this.btnDisableSel.Text = "Disable Selected";
            this.btnDisableSel.UseVisualStyleBackColor = true;
            this.btnDisableSel.Click += new System.EventHandler(this.btnDisableSel_Click);
            // 
            // btnRemoveSel
            // 
            this.btnRemoveSel.Location = new System.Drawing.Point(11, 151);
            this.btnRemoveSel.Name = "btnRemoveSel";
            this.btnRemoveSel.Size = new System.Drawing.Size(202, 23);
            this.btnRemoveSel.TabIndex = 3;
            this.btnRemoveSel.Text = "Remove Selected";
            this.btnRemoveSel.UseVisualStyleBackColor = true;
            this.btnRemoveSel.Click += new System.EventHandler(this.btnRemoveSel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numTier3);
            this.groupBox1.Controls.Add(this.numTier2);
            this.groupBox1.Controls.Add(this.numTier1);
            this.groupBox1.Controls.Add(this.numPrime);
            this.groupBox1.Location = new System.Drawing.Point(13, 235);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 123);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Level";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tier 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tier 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tier 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Prime";
            // 
            // numTier3
            // 
            this.numTier3.Location = new System.Drawing.Point(46, 97);
            this.numTier3.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numTier3.Name = "numTier3";
            this.numTier3.Size = new System.Drawing.Size(148, 20);
            this.numTier3.TabIndex = 3;
            this.numTier3.ValueChanged += new System.EventHandler(this.numTier3_ValueChanged);
            // 
            // numTier2
            // 
            this.numTier2.Location = new System.Drawing.Point(46, 71);
            this.numTier2.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numTier2.Name = "numTier2";
            this.numTier2.Size = new System.Drawing.Size(148, 20);
            this.numTier2.TabIndex = 2;
            this.numTier2.ValueChanged += new System.EventHandler(this.numTier2_ValueChanged);
            // 
            // numTier1
            // 
            this.numTier1.Location = new System.Drawing.Point(46, 45);
            this.numTier1.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numTier1.Name = "numTier1";
            this.numTier1.Size = new System.Drawing.Size(148, 20);
            this.numTier1.TabIndex = 1;
            this.numTier1.ValueChanged += new System.EventHandler(this.numTier1_ValueChanged);
            // 
            // numPrime
            // 
            this.numPrime.Location = new System.Drawing.Point(46, 19);
            this.numPrime.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numPrime.Name = "numPrime";
            this.numPrime.Size = new System.Drawing.Size(148, 20);
            this.numPrime.TabIndex = 0;
            this.numPrime.ValueChanged += new System.EventHandler(this.numPrime_ValueChanged);
            // 
            // numPPL
            // 
            this.numPPL.Location = new System.Drawing.Point(106, 183);
            this.numPPL.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numPPL.Name = "numPPL";
            this.numPPL.Size = new System.Drawing.Size(107, 20);
            this.numPPL.TabIndex = 5;
            this.numPPL.ValueChanged += new System.EventHandler(this.numPPL_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Points per Level";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Max Active Mutators";
            // 
            // numMaxMt
            // 
            this.numMaxMt.Location = new System.Drawing.Point(129, 209);
            this.numMaxMt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxMt.Name = "numMaxMt";
            this.numMaxMt.Size = new System.Drawing.Size(84, 20);
            this.numMaxMt.TabIndex = 8;
            this.numMaxMt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxMt.ValueChanged += new System.EventHandler(this.numMaxMt_ValueChanged);
            // 
            // ConfigureMutators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 368);
            this.Controls.Add(this.numMaxMt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numPPL);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRemoveSel);
            this.Controls.Add(this.mtListBox);
            this.Controls.Add(this.btnDisableSel);
            this.Controls.Add(this.mtGrp);
            this.Controls.Add(this.btnEnableSel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigureMutators";
            this.Text = "ConfigureMutators";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConfigureMutators_FormClosed);
            this.Load += new System.EventHandler(this.ConfigureMutators_Load);
            this.mtGrp.ResumeLayout(false);
            this.mtGrp.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTier3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTier2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTier1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxMt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox mtGrp;
        private System.Windows.Forms.Button btnRemoveSel;
        private System.Windows.Forms.Button btnDisableSel;
        private System.Windows.Forms.Button btnEnableSel;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.CheckedListBox mtListBox;
        private System.Windows.Forms.CheckBox newMutatorEnabled;
        private System.Windows.Forms.TextBox newMutatorName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numTier3;
        private System.Windows.Forms.NumericUpDown numTier2;
        private System.Windows.Forms.NumericUpDown numTier1;
        private System.Windows.Forms.NumericUpDown numPrime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numPPL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numMaxMt;
    }
}