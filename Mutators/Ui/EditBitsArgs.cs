﻿using Mutators.Model;

namespace Mutators.Ui
{
    public class EditBitsArgs
    {
        public bool Enable;
        public bool ClampedScaling;
        public Set Set;

        public EditBitsArgs(bool enable, bool clamped, Set set)
        {
            ClampedScaling = clamped;
            Set = set;
            Enable = enable;
        }
    }
}
