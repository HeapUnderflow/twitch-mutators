﻿namespace Mutators.Ui
{
    partial class ConfigureSets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureSets));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.newSetName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnNewSet = new System.Windows.Forms.Button();
            this.listSets = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRename = new System.Windows.Forms.Button();
            this.nameRename = new System.Windows.Forms.TextBox();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnLoadSelected = new System.Windows.Forms.Button();
            this.chkCopyCurrent = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCopyCurrent);
            this.groupBox1.Controls.Add(this.newSetName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnNewSet);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 86);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create New from Current";
            // 
            // newSetName
            // 
            this.newSetName.Location = new System.Drawing.Point(47, 19);
            this.newSetName.Name = "newSetName";
            this.newSetName.Size = new System.Drawing.Size(147, 20);
            this.newSetName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name";
            // 
            // btnNewSet
            // 
            this.btnNewSet.Location = new System.Drawing.Point(132, 52);
            this.btnNewSet.Name = "btnNewSet";
            this.btnNewSet.Size = new System.Drawing.Size(62, 23);
            this.btnNewSet.TabIndex = 2;
            this.btnNewSet.Text = "Add New";
            this.btnNewSet.UseVisualStyleBackColor = true;
            this.btnNewSet.Click += new System.EventHandler(this.btnNewSet_Click);
            // 
            // listSets
            // 
            this.listSets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listSets.FormattingEnabled = true;
            this.listSets.Location = new System.Drawing.Point(218, 15);
            this.listSets.Name = "listSets";
            this.listSets.Size = new System.Drawing.Size(290, 199);
            this.listSets.TabIndex = 1;
            this.listSets.SelectedIndexChanged += new System.EventHandler(this.listSets_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRename);
            this.groupBox2.Controls.Add(this.nameRename);
            this.groupBox2.Controls.Add(this.btnRemoveSelected);
            this.groupBox2.Controls.Add(this.btnLoadSelected);
            this.groupBox2.Location = new System.Drawing.Point(12, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 109);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "#";
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(136, 48);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(55, 23);
            this.btnRename.TabIndex = 7;
            this.btnRename.Text = "Rename";
            this.btnRename.UseVisualStyleBackColor = true;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // nameRename
            // 
            this.nameRename.Location = new System.Drawing.Point(6, 48);
            this.nameRename.Name = "nameRename";
            this.nameRename.Size = new System.Drawing.Size(124, 20);
            this.nameRename.TabIndex = 6;
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(6, 19);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(185, 23);
            this.btnRemoveSelected.TabIndex = 1;
            this.btnRemoveSelected.Text = "Remove Selected";
            this.btnRemoveSelected.UseVisualStyleBackColor = true;
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnLoadSelected
            // 
            this.btnLoadSelected.Location = new System.Drawing.Point(6, 74);
            this.btnLoadSelected.Name = "btnLoadSelected";
            this.btnLoadSelected.Size = new System.Drawing.Size(185, 23);
            this.btnLoadSelected.TabIndex = 0;
            this.btnLoadSelected.Text = "Load Selected";
            this.btnLoadSelected.UseVisualStyleBackColor = true;
            this.btnLoadSelected.Click += new System.EventHandler(this.btnLoadSelected_Click);
            // 
            // chkCopyCurrent
            // 
            this.chkCopyCurrent.AutoSize = true;
            this.chkCopyCurrent.Location = new System.Drawing.Point(9, 49);
            this.chkCopyCurrent.Name = "chkCopyCurrent";
            this.chkCopyCurrent.Size = new System.Drawing.Size(117, 30);
            this.chkCopyCurrent.TabIndex = 6;
            this.chkCopyCurrent.Text = "Copy Mutators from\r\nCurrent Set";
            this.chkCopyCurrent.UseVisualStyleBackColor = true;
            // 
            // ConfigureSets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 220);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.listSets);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigureSets";
            this.Text = "ConfigureSets";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConfigureSets_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox newSetName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNewSet;
        private System.Windows.Forms.ListBox listSets;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.TextBox nameRename;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnLoadSelected;
        private System.Windows.Forms.CheckBox chkCopyCurrent;
    }
}