﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mutators.Model;

namespace Mutators.Ui
{
    public partial class ConfigureBits : Form
    {

        private EditBitsArgs _args;
        private Action<EditBitsArgs> _callback;
        public ConfigureBits(EditBitsArgs args, Action<EditBitsArgs> callback)
        {
            InitializeComponent();
            Icon = Utils.RandIco();
            _args = args;
            _callback = callback;
        }

        private void ConfigureBits_Load(object sender, EventArgs e)
        {
            chkClampScaling.Checked = _args.ClampedScaling;
            chkEnable.Checked = _args.Enable;
            PopulateList();
        }

        private void btnAddBits_Click(object sender, EventArgs e)
        {
            var r = _args.Set.Bits.Values.Any(v => v.MinBits == numBits.Value)
                    && MessageBox.Show(
                        "There already exists an entry for that bit value\nOverride ?",
                        "Override ?",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning
                    ) == DialogResult.No;

            if (r) return;

            var bts = (int) numBits.Value;
            var pts = (int) numPoints.Value;

            var newbits = Bits.Create(bts, pts);
            // TODO: Fix ID Collision
            _args.Set.Bits.Add(newbits.Id, newbits);

            numBits.Value = numBits.Minimum;
            numPoints.Value = numPoints.Minimum;

            ClearSelection();
            PopulateList();
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < bitsList.Items.Count; i++)
            {
                if (!bitsList.GetItemChecked(i)) continue;

                var item = (Bits)bitsList.Items[i];
                _args.Set.Bits.Remove(item.Id);
            }

            ClearSelection();
            PopulateList();
        }

        private void ConfigureBits_FormClosed(object sender, FormClosedEventArgs e)
        {
            _callback.Invoke(_args);
        }

        private void ClearSelection()
        {
            for (var i = 0; i < bitsList.Items.Count; i++)
            {
                bitsList.SetItemChecked(i, false);
            }
        }

        private void PopulateList()
        {
            
            bitsList.Items.Clear();
            bitsList.Items.AddRange(_args.Set.Bits.Values.OrderBy(v => v.MinBits).ToArray());
        }

        private void chkClampScaling_CheckedChanged(object sender, EventArgs e)
        {
            _args.ClampedScaling = chkClampScaling.Checked;
        }

        private void chkEnable_CheckedChanged(object sender, EventArgs e)
        {
            _args.Enable = chkEnable.Checked;
        }
    }
}
