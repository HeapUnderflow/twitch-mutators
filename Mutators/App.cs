﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Mutators.Model;
using Mutators.Model.Exceptions;
using Mutators.Model.Extensions;
using Mutators.Ui;

namespace Mutators
{
    public partial class App : Form
    {
        private MutatorController _ctrl;
        private ConnectionStatus _state = ConnectionStatus.Disconnected;
        private bool _noise;

        private int _stateAvailMutators;
        private int _stateAvailBits;

        public App()
        {
            InitializeComponent();
            lboxLog.DoubleBuffering(true);
        }

        private void App_Load(object sender, EventArgs e)
        {
            if (!File.Exists("conf.json"))
            {
                Config.Save(new Config());
            }

            Config cfg;
            try
            {
                cfg = Config.Load();
            }
            catch (InvalidVersionException)
            {
                if (MessageBox.Show("The configuration is an invalid version.\nClear config ?",
                        "Config Version Mismatch", MessageBoxButtons.YesNo, MessageBoxIcon.Error) ==
                    DialogResult.OK)
                {
                    cfg = new Config();
                    Config.Save(cfg);
                }
                else
                {
                    Application.Exit();
                    return;
                }
            }
            catch (MalformedVersionException ex)
            {
                MessageBox.Show("Malformed version, the version is unreadable:\n" + ex.Message, "Malformed Version", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Application.Exit();
                return;
            }
            catch (MalformedConfigException ex)
            {
                MessageBox.Show("Malformed config:\n" + ex.Message, "Malformed Config", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Application.Exit();
                return;
            }

            _ctrl = new MutatorController(cfg);
            {
                var set = _ctrl.GetCurrentSet();
                _stateAvailMutators = set.Mutator.Count;
                _stateAvailBits = set.Bits.Count;
            }
            chnBox.Text = cfg.Channel;

            _ctrl.OnLog += (o, s) => { Log(s.Message, s.Level); };

            _ctrl.OnConnectionChanged += (o, status) =>
            {
                _state = status;
                if (status == ConnectionStatus.Connecting)
                {
                    Utils.SafeInvoke(btnStartStop, () =>
                    {
                        btnStartStop.Enabled = false;
                        btnStartStop.Text = "Starting...";
                    });
                    Utils.SafeInvoke(cnfGroup, () =>
                    {
                        btnCfgBits.Enabled = false;
                        btnCfgMutator.Enabled = false;
                        btnCfgMutatorSet.Enabled = false;
                        chnBox.Enabled = false;
                    });
                    Log("TWITCH => CONNECTING", LogLevel.Debug);
                }
                else if (status == ConnectionStatus.Disconnecting)
                {
                    Utils.SafeInvoke(btnStartStop, () =>
                    {
                        btnStartStop.Enabled = false;
                        btnStartStop.Text = "Stopping...";
                    });
                    Log("TWITCH => DISCONNECTING", LogLevel.Debug);
                }
                else if (status == ConnectionStatus.Connected)
                {
                    Utils.SafeInvoke(btnStartStop, () =>
                    {
                        btnStartStop.Enabled = true;
                        btnStartStop.Text = "Stop";
                    });
                    Log("TWITCH => CONNECTED");
                }
                else if (status == ConnectionStatus.Disconnected)
                {
                    Utils.SafeInvoke(btnStartStop, () =>
                    {
                        btnStartStop.Enabled = true;
                        btnStartStop.Text = "Start";
                    });
                    Utils.SafeInvoke(cnfGroup, () => {
                        btnCfgBits.Enabled = true;
                        btnCfgMutator.Enabled = true;
                        btnCfgMutatorSet.Enabled = true;
                        chnBox.Enabled = true;
                    });
                    Log("TWITCH => DISCONNECTED");
                }
                else
                {
                    Utils.SafeInvoke(btnStartStop, () =>
                    {
                        btnStartStop.Enabled = false;
                        btnStartStop.Text = "FATAL ERROR";
                    });
                    Log("A fatal error has occured while changing state.", LogLevel.Error);
                    Log("This should never happen, please report this to the Developer", LogLevel.Error);
                    Log("See the about info for a url", LogLevel.Error);
                }
            };

            _ctrl.OnMutatorGenerated += OnMutatorGenerated;
            _ctrl.OnPointsChanged += OnPointsChanged;
            SetAppState();
        }

        private void OnPointsChanged(object sender, int e)
        {
            Utils.SafeInvoke(nupPoints, () =>
            {
                _noise = true;
                nupPoints.Value = e;
                _noise = false;
            });
        }

        private void OnMutatorGenerated(object sender, MutatorGeneratedArgs e)
        {
            Utils.SafeInvoke(lblMutators, () =>
            {
                StringBuilder text = new StringBuilder();
                text.AppendLine("Current Mutators:");
                foreach (var mut in e.Active)
                {
                    text.AppendLine($"- {mut.Name}");
                }

                lblMutators.Text = text.ToString();
            });
        }

        public void Log(
            string message,
            LogLevel lvl = LogLevel.Information,
            bool shell_log = true,
            bool include_date = true,
            bool include_severity = true)
        {
            if (lvl < Utils.CurrentLogLevel) return;
            var severity = "";
            switch (lvl)
            {
                case LogLevel.Critical:
                    severity = "CRITICAL";
                    break;
                case LogLevel.Error:
                    severity = "ERROR";
                    break;
                case LogLevel.Warning:
                    severity = "WARNING";
                    break;
                case LogLevel.Information:
                    severity = "INFO";
                    break;
                case LogLevel.Debug:
                    severity = "DEBUG";
                    break;
                case LogLevel.Trace:
                    severity = "TRACE";
                    break;
                case LogLevel.None:
                default:
                    return;
            }

            var lstr = new StringBuilder();
            if (include_date)
            {
                lstr.Append($"[{DateTime.Now:HH:mm:ss}]");
            }

            if (include_severity)
            {
                lstr.Append($"[{severity:-8}]");
            }

            lstr.AppendFormat(" {0}", message);

            if (shell_log)
            {
                Console.WriteLine(lstr);
            }

            Utils.SafeInvoke(lboxLog, () =>
            {
                lboxLog.Items.Insert(0, lstr.ToString());

                if (lboxLog.Items.Count > 100)
                {
                    lboxLog.Items.RemoveAt(lboxLog.Items.Count - 1);
                }
            });
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            switch (_state)
            {
                case ConnectionStatus.Connected:
                    _ctrl.Stop();
                    break;
                case ConnectionStatus.Disconnected:
                    _ctrl.SaveConfiguration();
                    _ctrl.Start();
                    break;
            }
        }

        private void btnForceMutation_Click(object sender, EventArgs e)
        {
            _ctrl.ForceGeneration();
            //nupPoints.Value += 1;
        }

        private void nupPoints_ValueChanged(object sender, EventArgs e)
        {
            if (_noise) return;
            _ctrl.ForcePoints((int) nupPoints.Value);
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            new About().Show();
        }

        private void btnCfgMutator_Click(object sender, EventArgs e)
        {
            new ConfigureMutators(new EditMutatorArgs(_ctrl.GetCurrentSet(), _ctrl.GetSubPlans(), _ctrl.GetPPL(), _ctrl.GetMutatorLimit()), set =>
            {
                _ctrl.UpdateSet(set.Set);
                _ctrl.SetSubPlans(set.SubLevels);
                _ctrl.SetPPL(set.PPL);
                _ctrl.SetMutatorLimit(set.MaxMutators);
                _ctrl.SaveConfiguration();
                _stateAvailMutators = set.Set.Mutator.Count;
                SetAppState();
            }).ShowDialog();
        }

        private void btnCfgBits_Click(object sender, EventArgs e)
        {
           new ConfigureBits(new EditBitsArgs(_ctrl.BitsEnabled(), _ctrl.IsClamped(), _ctrl.GetCurrentSet()), (ebits) =>
           {
               _ctrl.UpdateSet(ebits.Set);
               _ctrl.SetClamped(ebits.ClampedScaling);
               _ctrl.SetBitsEnabled(ebits.Enable);
               _ctrl.SaveConfiguration();
               _stateAvailBits = ebits.Set.Bits.Count;
               SetAppState();
           }).ShowDialog();
        }

        private void btnCfgMutatorSet_Click(object sender, EventArgs e)
        {
            new ConfigureSets(new EditSetsArgs(_ctrl.GetAllSets(), _ctrl.GetSelectedId()), (args) =>
            {
                _ctrl.SetAllSets(args.Sets);
                _ctrl.SetSelectedId(args.Selected);
                _ctrl.SaveConfiguration();
                var set = _ctrl.GetCurrentSet();
                _stateAvailMutators = set.Mutator.Count;
                _stateAvailBits = set.Bits.Count;
                SetAppState();
            }).ShowDialog();
        }

        private void SetAppState()
        {
            Utils.SafeInvoke(appState, () =>
                appState.Text = $"Available Mutators: {_stateAvailMutators}\n"+
                                $"Available Bits: { _stateAvailBits}\n" +
                                $"Points needed for level: {_ctrl.GetPPL()}\n" +
                                $"Output File: {_ctrl.GetOutputFile()}");
        }

        private void chnBox_TextChanged(object sender, EventArgs e)
        {
            var v = chnBox.Text;
            if (v.StartsWith("#"))
            {
                v = v.Substring(1);
            }

            _ctrl.SetChannel(v);

        }

        private void chkEnableDebugOutput_CheckedChanged(object sender, EventArgs e)
        {
            Utils.CurrentLogLevel = chkEnableDebugOutput.Checked ? LogLevel.Debug : LogLevel.Information;
        }

        private void setOutFile_Click(object sender, EventArgs e)
        {
            if (storeFDiag.ShowDialog() != DialogResult.OK) return;
            var outf = storeFDiag.FileName;
            _ctrl.SetOutputFile(outf);
            SetAppState();
        }
    }
}
