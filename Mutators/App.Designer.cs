﻿namespace Mutators
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(App));
            this.lboxLog = new System.Windows.Forms.ListBox();
            this.lblAppStatus = new System.Windows.Forms.GroupBox();
            this.setOutFile = new System.Windows.Forms.Button();
            this.appState = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnForceMutation = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblMutators = new System.Windows.Forms.Label();
            this.nupPoints = new System.Windows.Forms.NumericUpDown();
            this.lblBitDesc = new System.Windows.Forms.Label();
            this.cnfGroup = new System.Windows.Forms.GroupBox();
            this.chkEnableDebugOutput = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chnBox = new System.Windows.Forms.TextBox();
            this.btnCfgMutatorSet = new System.Windows.Forms.Button();
            this.btnCfgBits = new System.Windows.Forms.Button();
            this.btnCfgMutator = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.storeFDiag = new System.Windows.Forms.SaveFileDialog();
            this.lblAppStatus.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupPoints)).BeginInit();
            this.cnfGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // lboxLog
            // 
            this.lboxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lboxLog.FormattingEnabled = true;
            this.lboxLog.Location = new System.Drawing.Point(6, 19);
            this.lboxLog.Name = "lboxLog";
            this.lboxLog.Size = new System.Drawing.Size(491, 342);
            this.lboxLog.TabIndex = 0;
            // 
            // lblAppStatus
            // 
            this.lblAppStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAppStatus.Controls.Add(this.setOutFile);
            this.lblAppStatus.Controls.Add(this.appState);
            this.lblAppStatus.Location = new System.Drawing.Point(12, 12);
            this.lblAppStatus.Name = "lblAppStatus";
            this.lblAppStatus.Size = new System.Drawing.Size(623, 100);
            this.lblAppStatus.TabIndex = 1;
            this.lblAppStatus.TabStop = false;
            this.lblAppStatus.Text = "App Status";
            // 
            // setOutFile
            // 
            this.setOutFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.setOutFile.Location = new System.Drawing.Point(567, 15);
            this.setOutFile.Name = "setOutFile";
            this.setOutFile.Size = new System.Drawing.Size(50, 73);
            this.setOutFile.TabIndex = 3;
            this.setOutFile.Text = "Set Output File";
            this.setOutFile.UseVisualStyleBackColor = true;
            this.setOutFile.Click += new System.EventHandler(this.setOutFile_Click);
            // 
            // appState
            // 
            this.appState.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.appState.Location = new System.Drawing.Point(7, 20);
            this.appState.Name = "appState";
            this.appState.Size = new System.Drawing.Size(554, 68);
            this.appState.TabIndex = 0;
            this.appState.Text = "// TO BE ADDED //";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lboxLog);
            this.groupBox2.Location = new System.Drawing.Point(339, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(504, 374);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartStop.Location = new System.Drawing.Point(642, 13);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(195, 45);
            this.btnStartStop.TabIndex = 1;
            this.btnStartStop.Text = "Start";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnForceMutation
            // 
            this.btnForceMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnForceMutation.Location = new System.Drawing.Point(641, 64);
            this.btnForceMutation.Name = "btnForceMutation";
            this.btnForceMutation.Size = new System.Drawing.Size(195, 48);
            this.btnForceMutation.TabIndex = 4;
            this.btnForceMutation.Text = "Force Mutation";
            this.btnForceMutation.UseVisualStyleBackColor = true;
            this.btnForceMutation.Click += new System.EventHandler(this.btnForceMutation_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.lblMutators);
            this.groupBox3.Controls.Add(this.nupPoints);
            this.groupBox3.Controls.Add(this.lblBitDesc);
            this.groupBox3.Location = new System.Drawing.Point(12, 119);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(315, 211);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mutator Status";
            // 
            // lblMutators
            // 
            this.lblMutators.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMutators.Location = new System.Drawing.Point(7, 48);
            this.lblMutators.Name = "lblMutators";
            this.lblMutators.Size = new System.Drawing.Size(302, 150);
            this.lblMutators.TabIndex = 2;
            this.lblMutators.Text = "Current Mutators: ";
            // 
            // nupPoints
            // 
            this.nupPoints.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nupPoints.Location = new System.Drawing.Point(88, 16);
            this.nupPoints.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nupPoints.Name = "nupPoints";
            this.nupPoints.Size = new System.Drawing.Size(221, 20);
            this.nupPoints.TabIndex = 1;
            this.nupPoints.ValueChanged += new System.EventHandler(this.nupPoints_ValueChanged);
            // 
            // lblBitDesc
            // 
            this.lblBitDesc.AutoSize = true;
            this.lblBitDesc.Location = new System.Drawing.Point(6, 18);
            this.lblBitDesc.Name = "lblBitDesc";
            this.lblBitDesc.Size = new System.Drawing.Size(76, 13);
            this.lblBitDesc.TabIndex = 0;
            this.lblBitDesc.Text = "Current Points:";
            // 
            // cnfGroup
            // 
            this.cnfGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cnfGroup.Controls.Add(this.chkEnableDebugOutput);
            this.cnfGroup.Controls.Add(this.label1);
            this.cnfGroup.Controls.Add(this.chnBox);
            this.cnfGroup.Controls.Add(this.btnCfgMutatorSet);
            this.cnfGroup.Controls.Add(this.btnCfgBits);
            this.cnfGroup.Controls.Add(this.btnCfgMutator);
            this.cnfGroup.Location = new System.Drawing.Point(13, 336);
            this.cnfGroup.Name = "cnfGroup";
            this.cnfGroup.Size = new System.Drawing.Size(314, 128);
            this.cnfGroup.TabIndex = 6;
            this.cnfGroup.TabStop = false;
            this.cnfGroup.Text = "Config";
            // 
            // chkEnableDebugOutput
            // 
            this.chkEnableDebugOutput.AutoSize = true;
            this.chkEnableDebugOutput.Location = new System.Drawing.Point(179, 70);
            this.chkEnableDebugOutput.Name = "chkEnableDebugOutput";
            this.chkEnableDebugOutput.Size = new System.Drawing.Size(129, 17);
            this.chkEnableDebugOutput.TabIndex = 6;
            this.chkEnableDebugOutput.Text = "Enable Debug Output";
            this.chkEnableDebugOutput.UseVisualStyleBackColor = true;
            this.chkEnableDebugOutput.CheckedChanged += new System.EventHandler(this.chkEnableDebugOutput_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Channel";
            // 
            // chnBox
            // 
            this.chnBox.Location = new System.Drawing.Point(57, 99);
            this.chnBox.Name = "chnBox";
            this.chnBox.Size = new System.Drawing.Size(251, 20);
            this.chnBox.TabIndex = 4;
            this.chnBox.TextChanged += new System.EventHandler(this.chnBox_TextChanged);
            // 
            // btnCfgMutatorSet
            // 
            this.btnCfgMutatorSet.Location = new System.Drawing.Point(9, 61);
            this.btnCfgMutatorSet.Name = "btnCfgMutatorSet";
            this.btnCfgMutatorSet.Size = new System.Drawing.Size(148, 32);
            this.btnCfgMutatorSet.TabIndex = 2;
            this.btnCfgMutatorSet.Text = "Manage Mutator-Sets";
            this.btnCfgMutatorSet.UseVisualStyleBackColor = true;
            this.btnCfgMutatorSet.Click += new System.EventHandler(this.btnCfgMutatorSet_Click);
            // 
            // btnCfgBits
            // 
            this.btnCfgBits.Location = new System.Drawing.Point(163, 20);
            this.btnCfgBits.Name = "btnCfgBits";
            this.btnCfgBits.Size = new System.Drawing.Size(145, 35);
            this.btnCfgBits.TabIndex = 1;
            this.btnCfgBits.Text = "Configure Bits";
            this.btnCfgBits.UseVisualStyleBackColor = true;
            this.btnCfgBits.Click += new System.EventHandler(this.btnCfgBits_Click);
            // 
            // btnCfgMutator
            // 
            this.btnCfgMutator.Location = new System.Drawing.Point(9, 20);
            this.btnCfgMutator.Name = "btnCfgMutator";
            this.btnCfgMutator.Size = new System.Drawing.Size(148, 35);
            this.btnCfgMutator.TabIndex = 0;
            this.btnCfgMutator.Text = "Configure Mutators";
            this.btnCfgMutator.UseVisualStyleBackColor = true;
            this.btnCfgMutator.Click += new System.EventHandler(this.btnCfgMutator_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAbout.Location = new System.Drawing.Point(13, 470);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(314, 22);
            this.btnAbout.TabIndex = 4;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // storeFDiag
            // 
            this.storeFDiag.DefaultExt = "txt";
            this.storeFDiag.FileName = "current_mutators.txt";
            this.storeFDiag.Filter = "Text files|*.txt|All files|*.*";
            this.storeFDiag.Title = "Output File Location";
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 504);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.cnfGroup);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnForceMutation);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblAppStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "App";
            this.Text = "Twitch Mutators";
            this.Load += new System.EventHandler(this.App_Load);
            this.lblAppStatus.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupPoints)).EndInit();
            this.cnfGroup.ResumeLayout(false);
            this.cnfGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lboxLog;
        private System.Windows.Forms.GroupBox lblAppStatus;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnForceMutation;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblBitDesc;
        private System.Windows.Forms.Label appState;
        private System.Windows.Forms.NumericUpDown nupPoints;
        private System.Windows.Forms.Label lblMutators;
        private System.Windows.Forms.GroupBox cnfGroup;
        private System.Windows.Forms.Button btnCfgMutatorSet;
        private System.Windows.Forms.Button btnCfgBits;
        private System.Windows.Forms.Button btnCfgMutator;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox chnBox;
        private System.Windows.Forms.CheckBox chkEnableDebugOutput;
        private System.Windows.Forms.Button setOutFile;
        private System.Windows.Forms.SaveFileDialog storeFDiag;
    }
}

