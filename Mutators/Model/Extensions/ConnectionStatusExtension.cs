﻿namespace Mutators.Model.Extensions
{
    public static class ConnectionStatusExtension
    {
        public static bool isActive(this ConnectionStatus status)
        {
            return status == ConnectionStatus.Connected || status == ConnectionStatus.Connecting || status == ConnectionStatus.Disconnecting;
        }

        public static bool isConnected(this ConnectionStatus status)
        {
            return status == ConnectionStatus.Connected;
        }
    }
}