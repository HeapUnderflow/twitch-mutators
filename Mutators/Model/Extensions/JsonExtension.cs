﻿using Mutators.Model.Exceptions;
using Newtonsoft.Json.Linq;

namespace Mutators.Model.Extensions
{
    public static class JsonExtension
    {
        public static T GetChecked<T>(this JObject o, string token)
        {
            if (!o.ContainsKey(token)) throw new MalformedConfigException($"Missing field {token}");

            var val = o[token].ToObject<T>();
            if (val == null)
            {
                throw new MalformedConfigException($"Empty values are not allowed: {token} == null");
            }

            return val;
        }

        public static JToken GetChecked(this JObject o, string token)
        {
            if (o.ContainsKey(token))
            {
                return o[token];
            }

            throw new MalformedConfigException($"Missing field {token}");
        }

        public static T GetChecked<T>(this JToken o, string token)
        {
            if (!o.HasValues || (o.Type == JTokenType.Object && !((JObject)o).ContainsKey(token)))
                throw new MalformedConfigException($"Missing field {token}");

            var val = o[token].ToObject<T>();
            if (val == null)
            {
                throw new MalformedConfigException($"Empty values are not allowed: {token} == null");
            }

            return val;

        }

        public static JToken GetChecked(this JToken o, string token)
        {
            if (!o.HasValues || (o.Type == JTokenType.Object && ((JObject)o).ContainsKey(token)))
            {
                return o[token];
            }

            throw new MalformedConfigException($"Missing field {token}");
        }
    }
}
