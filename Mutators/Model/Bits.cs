﻿using System;

namespace Mutators.Model
{
    public class Bits
    {
        public Guid Id;
        public int MinBits;
        public int PointValue;

        public static Bits Create(int mbits, int pval)
        {
            return new Bits(Guid.NewGuid(), mbits, pval);
        }

        public Bits(Guid id, int mbits, int pval)
        {
            MinBits = mbits;
            PointValue = pval;
            Id = id;
        }

        public override string ToString()
        {
            return $"{MinBits} Bits => {PointValue} Points";
        }
    }
}
