﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Mutators.Model
{
    public class Set
    {
        public readonly Guid Id;
        public string Name;
        
        public Dictionary<Guid, Mutator> Mutator;

        public Dictionary<Guid, Bits> Bits;

        public static Set Create(string name, List<Mutator> mt, List<Bits> bits)
        {
            var mut = new Dictionary<Guid, Mutator>();
            var bit = new Dictionary<Guid, Bits>();

            foreach (var mutator in mt)
            {
                mut.Add(mutator.Id, mutator);
            }

            foreach (var bitse in bits)
            {
                bit.Add(bitse.Id, bitse);   
            }

            return Set.Create(name, mut, bit);
        }

        public static Set Create(string name, Dictionary<Guid, Mutator> mutators, Dictionary<Guid, Bits> bits)
        {
            Guid newId = Guid.NewGuid();
            return new Set(newId, name, mutators, bits);
        }

        public static Set Empty(Guid id, string name)
        {
            return new Set(id, name, new Dictionary<Guid, Mutator>(), new Dictionary<Guid, Bits>());
        }

        public void Add(Mutator mt)
        {
            Mutator[mt.Id] = mt;
        }

        public void Remove(Mutator mt)
        {
            if (Mutator.ContainsKey(mt.Id))
            {
                Mutator.Remove(mt.Id);
            }
        }

        public void Remove(Guid id)
        {
            if (Mutator.ContainsKey(id))
            {
                Mutator.Remove(id);
            }
        }

        public void AddBits(Bits mt)
        {
            Bits[mt.Id] = mt;
        }

        public void RemoveBits(Bits mt)
        {
            if (Bits.ContainsKey(mt.Id))
            {
                Bits.Remove(mt.Id);
            }
        }

        public void RemoveBits(Guid id)
        {
            if (Bits.ContainsKey(id))
            {
                Bits.Remove(id);
            }
        }

        [JsonConstructor]
        public Set(Guid id, string name, Dictionary<Guid, Mutator> mt, Dictionary<Guid, Bits> bt)
        {
            Id = id;
            Name = name;
            Mutator = mt;
            Bits = bt;
        }
    }
}
