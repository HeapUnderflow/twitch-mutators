﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Mutators.Model.Exceptions;
using Mutators.Model.Extensions;
using Newtonsoft.Json.Linq;
using TwitchLib.Client.Enums;

namespace Mutators.Model
{
    public class Config
    {
        // Sets
        public Dictionary<Guid, Set> Sets = new Dictionary<Guid, Set>();

        // Misc Option
        public Guid Selected = Guid.Empty;
        public string OutputFile = "out.txt";
        public int PPL = 200;
        public int MutatorLimit = 3;
        public string Channel = "";

        // Flags
        public bool CapSingleLevel = false;
        public bool EnableBits = false;
        public bool ClampBits = false;

        // Sub Tiers
        //public int PRIME = 50;
        //public int T1 = 100;
        //public int T2 = 200;
        //public int T3 = 300;

        public Dictionary<SubscriptionPlan, int> SubTiers = new Dictionary<SubscriptionPlan, int>()
        {
            {SubscriptionPlan.Prime, 50},
            {SubscriptionPlan.Tier1, 100},
            {SubscriptionPlan.Tier2, 200},
            {SubscriptionPlan.Tier3, 300}
        };

        public static void Save(Config cfg, string file = "conf.json")
        {
            if (!cfg.Sets.ContainsKey(Guid.Empty))
            {
                var n = Set.Empty(Guid.Empty, "default");
                cfg.Sets.Add(n.Id, n);
            }

            using (var sw = new StreamWriter(File.Create(file)))
            {
                //sw.Write(JsonConvert.SerializeObject(cfg, Formatting.Indented));
                sw.Write(new JObject(
                    new JProperty("version", Utils.CurrentVersion().ToString()),
                    new JProperty("selected", cfg.Selected.ToString()),
                    new JProperty("output_file", cfg.OutputFile),
                    new JProperty("ppl", cfg.PPL),
                    new JProperty("mutator_limit", cfg.MutatorLimit),
                    new JProperty("channel", cfg.Channel),
                    new JProperty("cap_single_level", cfg.CapSingleLevel),
                    new JProperty("enable_bits", cfg.EnableBits),
                    new JProperty("clamp_bits", cfg.ClampBits),
                    new JProperty("sub_tiers", new JObject(
                        new JProperty("prime", cfg.SubTiers[SubscriptionPlan.Prime]),
                        new JProperty("tier1", cfg.SubTiers[SubscriptionPlan.Tier1]),
                        new JProperty("tier2", cfg.SubTiers[SubscriptionPlan.Tier2]),
                        new JProperty("tier3", cfg.SubTiers[SubscriptionPlan.Tier3])
                    )),
                    new JProperty("sets", new JArray(
                        from set in cfg.Sets.Values
                        select new JObject(
                            new JProperty("id", set.Id.ToString()),
                            new JProperty("name", set.Name),
                            new JProperty("mutators", new JArray(
                                    from mut in set.Mutator.Values
                                    select new JObject(
                                        new JProperty("id", mut.Id.ToString()),
                                        new JProperty("name", mut.Name),
                                        new JProperty("enabled", mut.Enabled)
                                    )
                                )
                            ),
                            new JProperty("bits", new JArray(
                                from mut in set.Bits.Values
                                select new JObject(
                                    new JProperty("id", mut.Id.ToString()),
                                    new JProperty("bval", mut.MinBits),
                                    new JProperty("pval", mut.PointValue)
                                )
                            ))
                        ))
                    )));
            }
        }

        public static Config Load(string file = "conf.json")
        {
            using (var sr = new StreamReader(File.OpenRead(file)))
            {
                var jcfg = JObject.Parse(sr.ReadToEnd());

                try
                {
                    if (Version.Parse(jcfg.GetChecked<string>("version")) != Utils.CurrentVersion())
                    {
                        throw new InvalidVersionException();
                    }
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    throw new MalformedVersionException("At least one component is less than zero.", ex);
                }
                catch (FormatException ex)
                {
                    throw new MalformedVersionException("At least one component is not an integer.", ex);
                }
                catch (ArgumentException ex)
                {
                    throw new MalformedVersionException(
                        "The version has fewer than two or more than four version components.", ex);
                }
                catch (OverflowException ex)
                {
                    throw new MalformedVersionException("", ex);
                }

                var cfg = new Config
                {
                    Selected = jcfg.GetChecked<Guid>("selected"),
                    OutputFile = jcfg.GetChecked<string>("output_file"),
                    PPL = jcfg.GetChecked<int>("ppl"),
                    MutatorLimit = jcfg.GetChecked<int>("mutator_limit"),
                    Channel = jcfg.GetChecked<string>("channel"),
                    CapSingleLevel = jcfg.GetChecked<bool>("cap_single_level"),
                    EnableBits = jcfg.GetChecked<bool>("enable_bits"),
                    ClampBits = jcfg.GetChecked<bool>("clamp_bits"),
                    SubTiers =
                    {
                        [SubscriptionPlan.Prime] = jcfg.GetChecked("sub_tiers").GetChecked<int>("prime"),
                        [SubscriptionPlan.Tier1] = jcfg.GetChecked("sub_tiers").GetChecked<int>("tier1"),
                        [SubscriptionPlan.Tier2] = jcfg.GetChecked("sub_tiers").GetChecked<int>("tier2"),
                        [SubscriptionPlan.Tier3] = jcfg.GetChecked("sub_tiers").GetChecked<int>("tier3")
                    }
                };

                foreach (var jset in jcfg.GetChecked("sets"))
                {
                    var id = jset.GetChecked<Guid>("id");
                    var name = jset.GetChecked<string>("name");
                    var mut = new Dictionary<Guid, Mutator>();
                    var bit = new Dictionary<Guid, Bits>();

                    foreach (var jmut in jset.GetChecked("mutators"))
                    {
                        var mutId = jmut.GetChecked<Guid>("id");
                        var mutName = jmut.GetChecked<string>("name");
                        var mutEnabled = jmut.GetChecked<bool>("enabled");

                        mut.Add(mutId, new Mutator(mutId, mutName, mutEnabled));
                    }

                    foreach (var jbit in jset.GetChecked("bits"))
                    {
                        var bitId = jbit.GetChecked<Guid>("id");
                        var bitMinbit = jbit.GetChecked<int>("bval");
                        var bitPoints = jbit.GetChecked<int>("pval");

                        bit.Add(bitId, new Bits(bitId, bitMinbit, bitPoints));
                    }

                    cfg.Sets.Add(id, new Set(id, name, mut, bit));
                }

                if (!cfg.Sets.ContainsKey(Guid.Empty))
                {
                    var n = Set.Empty(Guid.Empty, "default");
                    cfg.Sets.Add(n.Id, n);
                }

                return cfg;
            }
        }
    }
}