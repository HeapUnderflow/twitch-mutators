﻿using Microsoft.Extensions.Logging;

namespace Mutators.Model
{
    public class LogMessage
    {
        public readonly LogLevel Level;
        public readonly object Context;
        public readonly string Message;

        public LogMessage(LogLevel level, string message, object context = null)
        {
            Level = level;
            Message = message;
            Context = context;
        }
    }
}
