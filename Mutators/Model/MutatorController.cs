﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;
using Mutators.Model.Extensions;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Events;

namespace Mutators.Model
{
    public class MutatorController
    {
        private static readonly Random R = new Random((int) (DateTime.Now.Ticks - new Random().Next()));
        private readonly List<Mutator> _active = new List<Mutator>();
        private readonly List<Mutator> _available = new List<Mutator>();
        private readonly List<Mutator> _chosen = new List<Mutator>();
        private TwitchClient _client;

        private readonly Config _conf;
        private int _points;
        private ConnectionStatus _state = ConnectionStatus.Disconnected;

        private readonly Action<string> Log;
        public EventHandler<ConnectionStatus> OnConnectionChanged;
        public EventHandler<LogMessage> OnLog;
        public EventHandler<MutatorGeneratedArgs> OnMutatorGenerated;
        public EventHandler<int> OnPointsChanged;

        public MutatorController(Config cfg)
        {
            Log = s => OnLog.Invoke(this, new LogMessage(LogLevel.Information, s, this));
            _conf = cfg;

            if (!_conf.Sets.ContainsKey(_conf.Selected))
            {
                _conf.Selected = Guid.Empty;
            }
            
            _available = _conf.Sets[_conf.Selected].Mutator.Values.ToList();
        }

        public void Start()
        {
            if (_state.isActive()) return;

            if (_available.Count < _conf.MutatorLimit + 2)
            {
                Log("(Not Enough Mutators)");
                Log("You must construct additional Pylons !");
                return;
            }

            if (_conf.Channel.Length < 1)
            {
                Log("Uhhh... i... i cannot connect to nothing");
                Log("Please set a channel");
                return;
            }

            _client = new TwitchClient();
            _client.Initialize(new ConnectionCredentials("justinfan1234", ""), _conf.Channel);
            _client.OnConnected += ClientOnConnected;
            _client.OnDisconnected += ClientOnDisconnected;
            _client.OnNewSubscriber += ClientOnNewSubscriber;
            _client.OnReSubscriber += ClientOnReSubscriber;
            _client.OnGiftedSubscription += ClientOnGiftedSubscription;
            _client.OnMessageReceived += ClientOnMessageReceived;
            _client.OnLog += (sender, args) =>
            {
                OnLog.Invoke(sender, new LogMessage(LogLevel.Debug, args.Data, this));
            };

            _state = ConnectionStatus.Connecting;
            PushStateChange();
            _client.Connect();
        }

        public void Stop()
        {
            if (!_state.isActive()) return;
            _state = ConnectionStatus.Disconnecting;
            PushStateChange();
            _client.Disconnect();
        }

        public void SaveConfiguration(string file = "conf.json")
        {
            Config.Save(_conf, file);
        }

        public void WriteMutatorUpdate()
        {
            using (var sw = new StreamWriter(File.Create(_conf.OutputFile)))
            {
                sw.WriteLine($"{_points}/{_conf.PPL} Points");
                foreach (var m in _active) sw.WriteLine(m.Name);
            }
        }

        public void ForceGeneration()
        {
            GenerateMutators(_conf.PPL);
        }

        public void ForcePoints(int points)
        {
            _points = points;
            GenerateMutators(0);
        }

#region GETSET
        public Set GetCurrentSet()
        {
            return _conf.Sets[_conf.Selected];
        }

        public Dictionary<SubscriptionPlan, int> GetSubPlans()
        {
            return _conf.SubTiers;
        }

        public void SetSubPlans(Dictionary<SubscriptionPlan, int> plns)
        {
            if (plns.Keys.Any(v => v == SubscriptionPlan.Prime) 
                && plns.Keys.Any(v => v == SubscriptionPlan.Tier1) 
                && plns.Keys.Any(v => v == SubscriptionPlan.Tier2) 
                && plns.Keys.Any(v => v == SubscriptionPlan.Tier3))
            {
                _conf.SubTiers = plns;
            }
        }
    
        public void UpdateSet(Set set)
        {
            if (_conf.Sets.ContainsKey(set.Id))
            {
                _conf.Sets[set.Id] = set;
            }
            else
            {
                _conf.Sets.Add(set.Id, set);
            }

            if (set.Id != _conf.Selected) return;
            SetCurrentSet(_conf.Selected);
        }

        public void SetCurrentSet(Guid set)
        {
            _conf.Selected = set;
            _available.Clear();
            _available.AddRange(_conf.Sets[_conf.Selected].Mutator.Values.ToList());
        }

        public void SetChannel(string channel)
        {
            if (channel.StartsWith("#"))
            {
                channel = channel.Substring(1);
            }

            _conf.Channel = channel;
        }

        public int GetPPL()
        {
            return _conf.PPL;
        }

        public void SetPPL(int ppl)
        {
            _conf.PPL = ppl;
        }

        public string GetOutputFile()
        {
            return _conf.OutputFile;
        }

        public void SetOutputFile(string f)
        {
            if (f.Length < 1)
            {
                f = "out.txt";
            }

            _conf.OutputFile = f;
        }

        public bool IsClamped()
        {
            return _conf.ClampBits;
        }

        public void SetClamped(bool clamped)
        {
            _conf.ClampBits = clamped;
        }

        public bool BitsEnabled()
        {
            return _conf.EnableBits;
        }

        public void SetBitsEnabled(bool enabled)
        {
            _conf.EnableBits = enabled;
        }

        public int GetMutatorLimit()
        {
            return _conf.MutatorLimit;
        }

        public void SetMutatorLimit(int limit)
        {
            _conf.MutatorLimit = Utils.Clamp2(limit, 1, 100);
        }

        public Dictionary<Guid, Set> GetAllSets()
        {
            return _conf.Sets;
        }

        public void SetAllSets(Dictionary<Guid, Set> sets)
        {
            if (!sets.ContainsKey(Guid.Empty))
            {
                throw new InvalidDataException("Missing default set");
            }
            _conf.Sets = sets;
        }

        public Guid GetSelectedId()
        {
            return _conf.Selected;
        }

        public void SetSelectedId(Guid selected)
        {
            _conf.Selected = selected;
        }
#endregion

#region PRIVATE

        private void GenerateMutators(int points)
        {
            if (points == 0) return;

            if (_available.Count <= _conf.MutatorLimit + 2)
            {
                Log("(Not Enough Mutators)");
                Log("You must construct additional Pylons !");
                return;
            }

            var modified = false;
            _points += points;

            while (_points >= _conf.PPL)
            {
                if (_chosen.Count + _active.Count >= _available.Count) _chosen.Clear();

                Mutator m;
                do
                {
                    var n = R.Next(_available.Count);
                    m = _available[n];
                } while (
                    !m.Enabled
                    || _active.Any(v => v.Id == m.Id)
                    || _chosen.Any(v => v.Id == m.Id)
                );

                if (_active.Count >= _conf.MutatorLimit)
                {
                    var tmp = _active[0];
                    _active.RemoveAt(0);
                    _chosen.Add(tmp);
                }

                _active.Add(m);
                Log($"New Mutator: {m.Name}");

                modified = true;

                _points -= _conf.PPL;
                if (_conf.CapSingleLevel) break;
            }

            OnPointsChanged.Invoke(this, _points);
            if (modified) OnMutatorGenerated.Invoke(this, new MutatorGeneratedArgs(_active.ToArray()));

            WriteMutatorUpdate();
        }

        private int BitsToPoints(int bits)
        {
            if (!_conf.EnableBits || bits <= 0) return 0;

            var bts = _conf.Sets[_conf.Selected].Bits.Values.OrderBy(v => v.MinBits).ToList();

            if (bts[0].MinBits != 0) bts.Insert(0, new Bits(Guid.Empty, 0, 0));

            var i = 0;
            while (i < bts.Count && bts[i].MinBits <= bits)
            {
                i++;
            }

            i--;

            int pts;
            if (_conf.ClampBits)
            {
                pts = bts[i].PointValue;
            }
            else
            {
                var xI = i == bts.Count - 1 ? i - 1 : i;
                var yI = i == bts.Count - 1 ? i : i + 1;

                var xB = bts[xI];
                var yB = bts[yI];

                pts = (int) Math.Floor(
                    ((double) bits - xB.MinBits) /
                    ((double) yB.MinBits - xB.MinBits) *
                    yB.PointValue + xB.PointValue
                );
            }
            return pts;
        }

        private int SubtierToPoints(SubscriptionPlan plan)
        {
            switch (plan)
            {
                case SubscriptionPlan.NotSet:
                    Log("Encountered invalid subscription Plan");
                    return 0;
                case SubscriptionPlan.Prime:
                case SubscriptionPlan.Tier1:
                case SubscriptionPlan.Tier2:
                case SubscriptionPlan.Tier3:
                    return _conf.SubTiers[plan];
                default:
                    return 0;
            }
        }

        private void ClientOnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            if (_conf.EnableBits && e.ChatMessage.Bits > 0)
            {
                var pts = BitsToPoints(e.ChatMessage.Bits);
                Log(
                    $"{e.ChatMessage.DisplayName} cheered {e.ChatMessage.Bits} ({e.ChatMessage.BitsInDollars} $) => {pts} points.");
                GenerateMutators(pts);
            }
        }

        private void ClientOnGiftedSubscription(object sender, OnGiftedSubscriptionArgs e)
        {
            Log($"{e.Channel}: {e.GiftedSubscription.MsgParamRecipientDisplayName} subscribed with" +
                $" {e.GiftedSubscription.MsgParamSubPlan} [{e.GiftedSubscription.MsgParamMonths} months, gifted by {e.GiftedSubscription.DisplayName}]");
            GenerateMutators(SubtierToPoints(e.GiftedSubscription.MsgParamSubPlan));
        }

        private void ClientOnReSubscriber(object sender, OnReSubscriberArgs e)
        {
            Log($"{e.Channel}: {e.ReSubscriber.DisplayName} subscribed with" +
                $" {e.ReSubscriber.SubscriptionPlan} [{e.ReSubscriber.Months} month streak]");
            GenerateMutators(SubtierToPoints(e.ReSubscriber.SubscriptionPlan));
        }

        private void ClientOnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            Log($"{e.Channel}: {e.Subscriber.DisplayName} subscribed with {e.Subscriber.SubscriptionPlan} [new]");
            GenerateMutators(SubtierToPoints(e.Subscriber.SubscriptionPlan));
        }

        private void ClientOnDisconnected(object sender, OnDisconnectedEventArgs e)
        {
            _state = ConnectionStatus.Disconnected;
            PushStateChange();
            _client = null;
        }

        private void ClientOnConnected(object sender, OnConnectedArgs e)
        {
            _state = ConnectionStatus.Connected;
            PushStateChange();
        }

        private void PushStateChange()
        {
            OnConnectionChanged.Invoke(this, _state);
        }

#endregion
    }
}