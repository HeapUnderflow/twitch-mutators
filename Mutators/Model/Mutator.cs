﻿using System;

namespace Mutators.Model
{
    public class Mutator
    {
        public Guid Id;
        public string Name;
        public bool Enabled;

        public static Mutator Create(string name, bool enabled = true)
        {
            Guid newId = Guid.NewGuid();
            return new Mutator(newId, name, enabled);
        }

        public Mutator(Guid id, string name, bool enabled)
        {
            Id = id;
            Name = name;
            Enabled = enabled;
        }

        public string ToStringFull()
        {
            return $"{Name} {{ id: \"{Id}\", enabled: {Enabled} }}";
        }

        public override string ToString()
        {
            return $"{(Enabled ? "" : "[DISABLED]")} {Name}";
        }
    }
}
