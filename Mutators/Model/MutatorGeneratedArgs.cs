﻿using System.Collections.Generic;

namespace Mutators.Model
{
    public class MutatorGeneratedArgs
    {
        public List<Mutator> Active;
        public MutatorGeneratedArgs(params Mutator[] activeList)
        {
            Active = new List<Mutator>(activeList);
        }
    }
}
