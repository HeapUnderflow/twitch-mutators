﻿namespace Mutators.Model
{
    public enum ConnectionStatus
    {
        Disconnected,
        Disconnecting,
        Connected,
        Connecting,
        Unknown
    }
}
