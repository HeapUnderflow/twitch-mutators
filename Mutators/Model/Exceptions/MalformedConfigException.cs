﻿using System;
using System.Runtime.Serialization;

namespace Mutators.Model.Exceptions
{
    [Serializable]
    public class MalformedConfigException : System.Exception
    {
        public MalformedConfigException() : base()
        { }

        public MalformedConfigException(string message) : base(message)
        { }

        public MalformedConfigException(string message, System.Exception inner) : base(message, inner)
        { }

        public MalformedConfigException(SerializationInfo info, StreamingContext ctx) : base(info, ctx)
        { }
    }
}