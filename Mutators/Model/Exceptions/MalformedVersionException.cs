﻿using System;
using System.Runtime.Serialization;

namespace Mutators.Model.Exceptions
{
    [Serializable]
    class MalformedVersionException : System.Exception
    {
        public MalformedVersionException() : base()
        { }

        public MalformedVersionException(string message) : base(message)
        { }

        public MalformedVersionException(string message, System.Exception inner) : base(message, inner)
        { }

        public MalformedVersionException(SerializationInfo info, StreamingContext ctx) : base(info, ctx)
        { }
    }
}
