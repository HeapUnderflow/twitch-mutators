﻿using System;
using System.Runtime.Serialization;

namespace Mutators.Model.Exceptions
{
    [Serializable]
    class InvalidVersionException : System.Exception
    {
        public InvalidVersionException() : base()
        { }

        public InvalidVersionException(string message) : base(message)
        { }

        public InvalidVersionException(string message, System.Exception inner) : base(message, inner)
        { }

        public InvalidVersionException(SerializationInfo info, StreamingContext ctx) : base(info, ctx)
        { }
    }
}
