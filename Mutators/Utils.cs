﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Mutators.Properties;

namespace Mutators
{
    static class Utils
    {

        public static readonly Random R = new Random();

        public static LogLevel CurrentLogLevel = LogLevel.Information;

        /// <summary>
        /// Execute a method on the control's owning thread.
        /// </summary>
        /// <param name="uiElement">The control that is being updated.</param>
        /// <param name="updater">The method that updates uiElement.</param>
        /// <param name="forceSynchronous">True to force synchronous execution of 
        /// updater.  False to allow asynchronous execution if the call is marshalled
        /// from a non-GUI thread.  If the method is called on the GUI thread,
        /// execution is always synchronous.</param>
        public static void SafeInvoke(Control uiElement, Action updater, bool forceSynchronous = false)
        {
            if (uiElement == null)
            {
                throw new ArgumentNullException("uiElement");
            }

            if (uiElement.InvokeRequired)
            {
                if (forceSynchronous)
                {
                    uiElement.Invoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                }
                else
                {
                    uiElement.BeginInvoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                }
            }
            else
            {
                if (!uiElement.IsHandleCreated)
                {
                    // Do nothing if the handle isn't created already.  The user's responsible
                    // for ensuring that the handle they give us exists.
                    return;
                }

                if (uiElement.IsDisposed)
                {
                    throw new ObjectDisposedException("Control is already disposed.");
                }

                updater();
            }
        }

        public static Icon RandIco()
        {
            switch (R.Next(3))
            {
                case 0:
                    return Icons.ico_deccHypers;
                case 1:
                    return Icons.ico_deccWoah;
                default:
                    return Icons.ico_deccCult;
            }
        }

        public static Bitmap RandImg()
        {
            switch (R.Next(3))
            {
                case 0:
                    return Icons.deccWoah;
                case 1:
                    return Icons.deccHypers;
                default:
                    return Icons.deccCult;
            }
        }

        public static int Clamp2(int v, int l, int h)
        {
            if (v < l) return l;
            if (v > h) return h;
            return v;
        }

        public static Version CurrentVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
